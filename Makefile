all: convert

download:
	-wget -nv -nc http://www.polantis.com/data/2/2/1093/formats/14/90/IKEA-Expedit_Bookcase-3d.aof
	-wget -nv -nc http://www.polantis.com/data/2/2/1093/formats/16/95/IKEA-Expedit_Bookcase-3d.obj \
	     -O IKEA-Expedit_Bookcase-3d.obj.orig
	
	-wget -nv -nc http://www.polantis.com/data/2/2/1094/formats/14/90/IKEA-Expedit_Bookcase_Black-3d.aof
	-wget -nv -nc http://www.polantis.com/data/2/2/1094/formats/16/95/IKEA-Expedit_Bookcase_Black-3d.obj \
	     -O IKEA-Expedit_Bookcase_Black-3d.obj.orig

convert: download
	@for file in *.aof; do ./aof2obj.py "$$file"; done
	@for file in *.prw; do ./prw2ppm.py "$$file"; done

clean:
	rm -f *.obj *.prw *.ppm *~

cleanall: clean
	-rm -i *.aof *.obj.orig
